const env = {  
  'API_HOST': 'http://api.localhost:8888',
  // 'API_HOST': 'http://api.localhost:8888',
  'APP_NAME': 'Demo',
  'APP_DESC': 'This is a  demo',
  'APP_KEYWORDS': 'Demo, Shop',
  'DEBUG': false,
  'ENCRYPTED': false,
  'VERSION': '1.0.0', // API版本号
}